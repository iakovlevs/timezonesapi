from datetime import date, datetime, timedelta, timezone

import pytest
import requests

class TestFirstAPI:
   timeZones = [
      ("Europe/Amsterdam")
   ]


   @pytest.mark.parametrize('timeZone', timeZones)
   def test_get_timezone(self, timeZone):
      url = "https://timeapi.io/api/Time/current/zone?timeZone="+ timeZone

      timeZone = {'name':timeZone}

      response = requests.get(url, params=timeZone)

      assert response.status_code == 200, "Wrong response code"

      response_body = response.json()
      data = date.today()
      data2 = datetime.now()
      print("MS", data2.microsecond)
      assert response_body["year"] == int(data.strftime("%Y"))
      assert response_body["month"] == int(data.strftime("%m"))
      assert response_body["day"] == int(data.strftime("%d"))
      assert response_body["hour"] == int(data2.strftime("%H"))-2
      assert response_body["minute"] == int(data2.strftime("%M"))
      assert response_body["seconds"] == int(data2.strftime("%S"))
      # assert response_body["milliSeconds"] == int(data.strftime("%d"))
      assert response_body["timeZone"] == "Europe/Amsterdam"
      # assert  "answer" in response_dict, "Correct"
      # expected_response_text = "dateTime"
      # actual_response_text = response_dict["dateTime"]
      # assert actual_response_text == expected_response_text, "Not correct"